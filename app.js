'use strict';

const express = require('express');
const url = require('url');
const http = require('https');
const {proxyFetch, ImageProxyServer, ImageProxyDataType} =
      require("nft-image-proxy");

let port = process.env.npm_config_port;
let apikey = process.env.npm_config_cryptonomickey;
const app = express();

const imageProxy = {
  url: "https://imgproxy-prod.cryptonomic-infra.tech",
  version: "1.0.0",
  apikey: apikey,
};

// Create an instance of the http server to handle HTTP requests
app.get('/', async function(req, res) {
  // Set a response type of plain text for the response

  try {
    let params = url.parse(req.url,true).query;

    if (!params.url) {
      throw Error('missing url parameter');
    }

    let urlParam = decodeURIComponent(params.url);

    let result = await
    proxyFetch(
      imageProxy,
      urlParam,
      ImageProxyDataType.Json,
      false
    );

    let imgString = result.result.data;
    let base64ContentArray = imgString.split(",");

    // base64 encoded data - pure
    let mimeType =
        base64ContentArray[0]
        .substring(base64ContentArray[0].indexOf(":")+1
                   , base64ContentArray[0].indexOf(";"));

    let base64Data = base64ContentArray[1];

    let img = Buffer.from(base64Data, 'base64');

    res.writeHead(200, {
      'Content-Type': mimeType,
      'Content-Length': img.length
    }); 

    // Send back a response and end the connection
    res.end(img);

  } catch (error){
    console.log(error);
  }

});

app.get('/check', function (req, response) {

  http.get(imageProxy.url, result => {
    if (result.statusCode == 200) {
      response.end("proxy available");
    } else {
      response.status(400);
      response.end("proxy unavailable");
    }
  }).on('error', e => {
    console.log(e);
    response.status(400);
    response.end("proxy unavailable");
  });


});


app.listen(port);
console.log(`Running on port ${port}`);
